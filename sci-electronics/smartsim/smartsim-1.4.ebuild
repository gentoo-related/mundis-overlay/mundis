# Copyright 1999-2012 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=8
inherit autotools desktop git-r3

DESCRIPTION="SmartSim is a free and open source digital logic circuit design and simulation package"
EGIT_REPO_URI="https://github.com/ashleynewson/SmartSim.git"
EGIT_COMMIT="v${PV}"

SLOT="0"
LICENSE="GPL-3"
KEYWORDS="~x86 ~amd64"
DEPEND=""
RDEPEND="${DEPEND}"

src_prepare() {
	default
	eautoreconf
}

src_install() {
        make_desktop_entry ${PN} "SmartSim" /usr/share/smartsim/images/icons/smartsim64.png
	default
}
