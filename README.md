Adding the overlay
------------------
##### Using app-eselect/eselect-repository
eselect repository add mundis git https://gitlab.com/gentoo-related/mundis-overlay/mundis.git  
emerge --sync  
##### If you really wanna use app-portage/layman
uncomment in: `/etc/layman/layman.cfg` the line  
`# overlay_defs : /etc/layman/overlays` to  
`overlay_defs : /etc/layman/overlays`  
and use the following commands  
`wget -P /etc/layman/overlays/ https://gitlab.com/gentoo-related/mundis-overlay/scripts-files/-/raw/main/mundis.xml`  
`layman -L`  
`layman -a mundis`  

##### Ebuilds Overview:

<table>
<tr><td>
<a href=https://gitlab.com/gentoo-related/mundis-overlay/mundis/-/tree/main/app-benchmarks/glmark2>app-benchmarks/glmark2</a>
</td><td>
Opengl test suite
</td></tr>
<tr><td>
<a href=https://gitlab.com/gentoo-related/mundis-overlay/mundis/-/tree/main/sci-electronics/smartsim>sci-electronics/smartsim</a>
</td><td>
SmartSim is a free and open source digital logic circuit design and simulation package
</td></tr>
<tr><td>
<a href=https://gitlab.com/gentoo-related/mundis-overlay/mundis/-/tree/main/x11-misc/xdgmenumaker>x11-misc/xdgmenumaker</a>
</td><td>
Command line tool that generates XDG menus for several window managers
</td></tr>
<tr><td>
<a href=https://gitlab.com/gentoo-related/mundis-overlay/mundis/-/tree/main/x11-misc/menumaker>x11-misc/menumaker</a>
</td><td>
Utility that scans through the system and generates a menu of installed programs
</td></tr>
<tr><td>
<a href=https://gitlab.com/gentoo-related/mundis-overlay/mundis/-/tree/main/media-gfx/mtpaint>media-gfx/mtpaint</a>
</td><td>
Simple gtk+ painting program
</td></tr>
</table>
